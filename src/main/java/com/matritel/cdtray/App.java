package com.matritel.cdtray;

import java.time.LocalDate;
import java.util.*;

public class App {
    public static void main(String[] args) {
        //here we create 2 tracks
        Track t1 = new Track("Moonland", 256);
        Track t2 = new Track("Midnight Man", 302);

        //the tracks are added to a list
        List<Track> nickCave = new ArrayList<>();
        nickCave.add(t1);
        nickCave.add(t2);
        LocalDate localDate2 = LocalDate.of(2015, 12, 31);

        //the tracklist is added to a CD
        CD cd1 = new CD("Do you love me", "Nick Cave", LocalDate.of(1995, 10, 25), nickCave);

        //the CD is added to a CDTray
        List<CD> nickCaveCDs = new ArrayList<>();
        nickCaveCDs.add(cd1);
        CDTray pop = new CDTray("Pop", nickCaveCDs);

        //CDTray is added to a catalog
        Map<String, CDTray> catalogInfo = new HashMap<>();
        catalogInfo.put("Tray1", pop);

        //Here we create another CdTRay called rock
        CDTray rock = new CDTray("Rock", Arrays.asList(new CD(
                "Ten", "Pearl Jam", LocalDate.now(), Arrays.asList(new Track("Jeremy", 120), new Track("Release", 120))
        ), new CD(
                "In utero", "Nirvana", LocalDate.now(), Arrays.asList(new Track("Rape me", 120),
                new Track("Come as you are", 120)))));

        //CDTray rock is added to catalog
        catalogInfo.put("Rock", rock);

        //a sexond catalgo is created called "catalog"
        Catalog catalog = new Catalog(catalogInfo);

        //we add CDtrays to our second catalog
        catalog.addTray("Zsolt CDS", new CDTray("Zsolt favs", Arrays.asList(new CD("Smoke", "Deep Purple", localDate2, Arrays.asList(new Track("Smoke on The Water", 256), new Track("Fire on the Sky", 302))))));
        catalog.addTray("Rock", new CDTray("Rocky", Arrays.asList(new CD("Music", "Nine Inch Nails", localDate2, Arrays.asList(new Track("Mash", 496), new Track("Hire", 158))))));
        catalog.addTray("Pop", new CDTray("Popy", Arrays.asList(new CD("Music2", "Nine Inch Nails", localDate2, Arrays.asList(new Track("Rash", 496), new Track("Fire", 158))))));

        //Here we test the methods written to find ot different information from our catalogs
        System.out.println(catalog.sumDurationCatalog());
        System.out.println(catalog.sumDurationOfTray("Rock"));
        System.out.println(catalog.sumDurationCD("Smoke"));
        System.out.println(catalog.searchByCDTitle("Do you love me"));
        System.out.println(catalog.searchByArtist("Nick Cave"));
        System.out.println(catalog.searchByArtist("Nine Inch Nails"));
        System.out.println(catalog.searchByArtistWithAllMatches("Nine Inch Nails"));
        System.out.println(catalog.searchByTrackName("Moonland"));
        System.out.println(catalog.searchByTrackName("Release"));
        System.out.println(catalog.searchForLocationByTitle("Do you love me"));
        System.out.println(catalog.searchForLocationByArtist("Nick Cave"));
        System.out.println(catalog.searchForLocationByTrackName("Moonland"));
        System.out.println(catalog.searchForLocationByTitle("In utero"));
        catalog.printCDCover("Music");
    }

}


