package com.matritel.cdtray;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CD {
    private String title;
    private String artist;
    private LocalDate releaseDate;
    private List<Track> listOfTracks = new ArrayList();

    CD(String title, String artist, LocalDate releaseDate, List<Track> listOfTracks) {

        this.title = title;
        this.artist = artist;
        this.releaseDate = releaseDate;
        this.listOfTracks = listOfTracks;
    }

    String getTitle() {
        return title;
    }

    String getArtist() {
        return artist;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    List<Track> getListOfTracks() {
        return listOfTracks;
    }

    @Override
    public String toString() {
        return "CD{" +
                "title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", releaseDate=" + releaseDate +
                ", listOfTracks=" + listOfTracks +
                '}';
    }

}
