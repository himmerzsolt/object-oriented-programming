package com.matritel.cdtray;

import java.util.ArrayList;
import java.util.List;

public class CDTray {
    private List<CD> listOfCDs = new ArrayList<>();
    private String cdTrayLabel;

    CDTray(String label, List<CD> listOfCDs) {
        this.cdTrayLabel = label;
        this.listOfCDs = listOfCDs;
    }

    void addCD(CD cd) {
        listOfCDs.add(cd);
    }

    String getCdTrayLabel() {
        return cdTrayLabel;
    }

    List<CD> getListOfCDs() {
        return listOfCDs;
    }

    @Override
    public String toString() {
        return "CDTray{" +
                "cdTrayLabel='" + cdTrayLabel + '\'' +
                ", listOfCDs=" + listOfCDs +
                '}';
    }


}
