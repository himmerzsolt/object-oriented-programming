package com.matritel.cdtray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Catalog {
    private Map<String, CDTray> mapOfTrays = new HashMap<String, CDTray>();

    Catalog(Map<String, CDTray> mapOfTrays) {
        this.mapOfTrays = mapOfTrays;
    }

    void addTray(String cdTrayLabel, CDTray cdTray) {
        mapOfTrays.put(cdTrayLabel, cdTray);
    }

    CD searchByCDTitle(String title) {
//       create CD instance with value
        CD cd = null;
//        Iterate trough the HashMap to find the CDTray object
        for (String key : mapOfTrays.keySet()) {
//            CDTray instance will be this element of the HashMap
            CDTray cdTray = mapOfTrays.get(key);
//            Iterate through the ArrayList of CD-s to find the CD
            for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
//                If the element of the list equal with title
                if (cdTray.getListOfCDs().get(i).getTitle().equals(title)) {
//                    then the CD instance will be this element
                    cd = cdTray.getListOfCDs().get(i);
                }
//                give back the element found
            }
        }
        return cd;
    }


    CD searchByTrackName(String trackName) {
        CD cd = null;
        for (String key : mapOfTrays.keySet()) {
            CDTray cdTray = mapOfTrays.get(key);
            for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
                for (int j = 0; j < cdTray.getListOfCDs().get(i).getListOfTracks().size(); j++) {
                    if (cdTray.getListOfCDs().get(i).getListOfTracks().get(j).getTrackName().equals(trackName)) {
                        cd = cdTray.getListOfCDs().get(i);
                    }
                }
            }
        }
        return cd;
    }

    CD searchByArtist(String artist) {
        CD cd = null;
        for (String key : mapOfTrays.keySet()) {
            CDTray cdTray = mapOfTrays.get(key);
            for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
                if (cdTray.getListOfCDs().get(i).getArtist().equals(artist)) {
                    cd = cdTray.getListOfCDs().get(i);
                }
            }
        }
        return cd;
    }

    List<CD> searchByArtistWithAllMatches(String artist) {
        CD cd = null;
        List<CD> listMatchedCds = new ArrayList<>();
        for (String key : mapOfTrays.keySet()) {
            CDTray cdTray = mapOfTrays.get(key);
            for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
                if (cdTray.getListOfCDs().get(i).getArtist().equals(artist)) {
                    cd = cdTray.getListOfCDs().get(i);
                    listMatchedCds.add(cd);
                }
            }
        }
        return listMatchedCds;
    }

    Map<String, Integer> searchForLocationByTitle(String title) {
        Map<String, Integer> locationOfCD = new HashMap<String, Integer>();
        for (String key : mapOfTrays.keySet()) {
            CDTray cdTray = mapOfTrays.get(key);
            for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
                if (cdTray.getListOfCDs().get(i).getTitle().equals(title)) {
                    locationOfCD.put(key, i);
                }
            }
        }
        return locationOfCD;
    }

    Map<String, Integer> searchForLocationByTrackName(String trackName) {
        Map<String, Integer> locationOfCD = new HashMap<String, Integer>();
        for (String key : mapOfTrays.keySet()) {
            CDTray cdTray = mapOfTrays.get(key);
            for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
                for (int j = 0; j < cdTray.getListOfCDs().get(i).getListOfTracks().size(); j++) {
                    if (cdTray.getListOfCDs().get(i).getListOfTracks().get(j).getTrackName().equals(trackName)) {
                        locationOfCD.put(key, i);
                    }
                }
            }
        }
        return locationOfCD;
    }

    Map<String, Integer> searchForLocationByArtist(String artist) {
        Map<String, Integer> locationOfCD = new HashMap<String, Integer>();
        for (String key : mapOfTrays.keySet()) {
            CDTray cdTray = mapOfTrays.get(key);
            for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
                if (cdTray.getListOfCDs().get(i).getArtist().equals(artist)) {
                    locationOfCD.put(key, i);
                }
            }
        }
        return locationOfCD;
    }

    int sumDurationCD(String title) {
        int sum = 0;
        CD cd = searchByCDTitle(title);
        for (int i = 0; i < cd.getListOfTracks().size(); i++) {
            sum += cd.getListOfTracks().get(i).getDuration();
        }
        return sum;
    }

    int sumDurationCatalog() {
        int sum = 0;
        String titleOfCD = null;
        for (String key : mapOfTrays.keySet()) {
            CDTray cdTray = mapOfTrays.get(key);

            for (int i = 0; i < cdTray.getListOfCDs().size(); i++)
                titleOfCD = cdTray.getListOfCDs().get(i).getTitle();
            sum += sumDurationCD(titleOfCD);
        }
        return sum;
    }

    int sumDurationOfTray(String cdTrayLabel) {
        int sum = 0;
        String titleOfCD;
        CDTray cdTray = mapOfTrays.get(cdTrayLabel);
        for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
            titleOfCD = cdTray.getListOfCDs().get(i).getTitle();
            sum += sumDurationCD(titleOfCD);
        }
        return sum;
    }

    void printCDCover(String title) {
        CD cd = null;
        for (String key : mapOfTrays.keySet()) {
            CDTray cdTray = mapOfTrays.get(key);
            for (int i = 0; i < cdTray.getListOfCDs().size(); i++) {
                if (cdTray.getListOfCDs().get(i).getTitle().equals(title)) {
                    cd = cdTray.getListOfCDs().get(i);
                    System.out.println(cdTray.getListOfCDs().get(i).getArtist() + " - " + cdTray.getListOfCDs().get(i).getTitle());
                    for (int j = 0; j < cdTray.getListOfCDs().get(i).getListOfTracks().size(); j++) {
                        int mnts = cdTray.getListOfCDs().get(i).getListOfTracks().get(j).getDuration() / 60;
                        int secs = cdTray.getListOfCDs().get(i).getListOfTracks().get(j).getDuration() % 60;
                        System.out.println(j + 1 + " - " + cdTray.getListOfCDs().get(i).getListOfTracks().get(j).getTrackName() + " - " + String.format("%02d:%02d", mnts, secs));
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "mapOfTrays=" + mapOfTrays +
                '}';
    }
}
