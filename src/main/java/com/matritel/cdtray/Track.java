package com.matritel.cdtray;

public class Track {
    private String trackName;
    private int duration;

    Track(String trackName, int duration) {
        this.trackName = trackName;
        this.duration = duration;

    }

    String getTrackName() {
        return trackName;
    }

    int getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return "Track{" +
                "trackName='" + trackName + '\'' +
                ", duration=" + duration +
                '}';
    }

}
