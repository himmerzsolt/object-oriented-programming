package com.matritel.rpg_game;

import com.matritel.rpg_game.board.Board;

import javax.swing.*;

public class App {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        Board myBoard = new Board();
        frame.add(myBoard);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

