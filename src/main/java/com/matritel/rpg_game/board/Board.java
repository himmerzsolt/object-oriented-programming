package com.matritel.rpg_game.board;

import com.matritel.rpg_game.character.Boss;
import com.matritel.rpg_game.character.Hero;
import com.matritel.rpg_game.character.Skeleton;
import com.matritel.rpg_game.util.Dice;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class Board extends JComponent implements KeyListener {

    private static final int WALL = 0;
    private static final int FLOOR = 1;
    private static final int INITIAL_FLOOR = 2;
    private Dice dice;
    private Hero hero = new Hero(0, 0);
    private Boss boss = new Boss(0, 0);
    private int levelOfArea = 1;
    private Skeleton skeleton1 = new Skeleton(0, 0, levelOfArea);
    private Skeleton skeleton2 = new Skeleton(0, 0, levelOfArea);
    private ArrayList<int[][]> listOfBoards = new ArrayList<>();
    private int[][] board1 = createBoard();
    private int[][] board2 = createBoard();
    private int[][] board3 = createBoard();
    private ArrayList<GameObject> gameObjects1 = new ArrayList<>();
    private ArrayList<GameObject> gameObjects2 = new ArrayList<>();
    private ArrayList<GameObject> gameObjects3 = new ArrayList<>();
    private List<Skeleton> skeletonList = new ArrayList<>();

    public Board() {
        init();
        drawBoard(board1, gameObjects1);
        drawBoard(board2, gameObjects2);
        drawBoard(board3, gameObjects3);
    }

    @Override
    public void paint(Graphics graphics) {
        // here you have a 600x600 canvas
        // you can create and draw an image using the com.matritel.rpg_game.board1.GameObject class below
        // e.g.
        switch (levelOfArea) {
            case 1:
                for (GameObject gameObject : gameObjects1) {
                    gameObject.draw(graphics);
                }
                drawCharacters(skeletonList, graphics);
                drawTexts(skeletonList, graphics);
                break;
            case 2:
                for (GameObject gameObject : gameObjects2) {
                    gameObject.draw(graphics);
                }
                drawCharacters(skeletonList, graphics);
                drawTexts(skeletonList, graphics);
                break;
            case 3:
                for (GameObject gameObject : gameObjects3) {
                    gameObject.draw(graphics);
                }
                drawCharacters(skeletonList, graphics);
                drawTexts(skeletonList, graphics);
                break;
        }
        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            hero.changePic("down");
            hero.move(0, 1, listOfBoards.get(levelOfArea - 1));
            for (Skeleton s : skeletonList
            ) {
                s.move(listOfBoards.get(levelOfArea - 1));
            }
            boss.move(listOfBoards.get(levelOfArea - 1));
        } else if (e.getKeyCode() == KeyEvent.VK_UP) {
            hero.changePic("up");
            hero.move(0, -1, listOfBoards.get(levelOfArea - 1));
            for (Skeleton s : skeletonList
            ) {
                s.move(listOfBoards.get(levelOfArea - 1));
            }
            boss.move(listOfBoards.get(levelOfArea - 1));
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            hero.changePic("right");
            hero.move(1, 0, listOfBoards.get(levelOfArea - 1));
            for (Skeleton s : skeletonList
            ) {
                s.move(listOfBoards.get(levelOfArea - 1));
            }
            boss.move(listOfBoards.get(levelOfArea - 1));
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            hero.changePic("left");
            hero.move(-1, 0, listOfBoards.get(levelOfArea - 1));
            for (Skeleton s : skeletonList
            ) {
                s.move(listOfBoards.get(levelOfArea - 1));
            }
            boss.move(listOfBoards.get(levelOfArea - 1));
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            for (Skeleton s : skeletonList
            ) {
                if (hero.getPosY() == s.getPosY() && hero.getPosX() == s.getPosX() && hero.getHealthPoint() > 0) {
                    hero.strike(s);
                    if (s.getHealthPoint() <= 0) {
                        hero.setLevel(hero.getLevel() + 1);
                        int d6 = dice.doRandom(6) + 1;
                        hero.setMax_hp(hero.getMaxHP() + d6);
                        hero.setDefendPoint(hero.getDefendPoint() + d6);
                        hero.setStrikePoint(hero.getStrikePoint() + d6);
                        hero.setLevel(hero.getLevel() + 1);
                        if (s.hasKey() && levelOfArea < 3) {
                            this.levelOfArea = levelOfArea + 1;
                            hero.resetPosition(listOfBoards.get(levelOfArea - 1));
                            boss.reset(listOfBoards.get(levelOfArea - 1), levelOfArea);
                            for (Skeleton sk : skeletonList
                            ) {
                                sk.reset(listOfBoards.get(levelOfArea - 1), levelOfArea);
                            }
                            Skeleton extraSkeleton = new Skeleton(0, 0, levelOfArea);
                            extraSkeleton.reset(listOfBoards.get(levelOfArea - 1), levelOfArea);
                            skeletonList.add(extraSkeleton);
                            setKeyForSkeletons(skeletonList);
                            break;
                        }
                        break;
                    } else s.strike(hero);
                    break;
                }
            }
            if (hero.getPosX() == boss.getPosX() && hero.getPosY() == boss.getPosY() && hero.getHealthPoint() > 0) {
                hero.strike(boss);
                if (boss.getHealthPoint() <= 0) {
                    int d6 = dice.doRandom(6) + 1;
                    hero.setMax_hp(hero.getMaxHP() + d6);
                    hero.setDefendPoint(hero.getDefendPoint() + d6);
                    hero.setStrikePoint(hero.getStrikePoint() + d6);
                    hero.setLevel(hero.getLevel() + 1);
                }
                boss.strike(hero);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void init() {
        this.dice = new Dice();
        hero.resetPosition(board1);
        skeleton1.reset(board1, 1);
        skeleton2.reset(board1, 1);
        boss.reset(board1, 1);
        listOfBoards.add(board1);
        listOfBoards.add(board2);
        listOfBoards.add(board3);
        this.setFocusable(true);
        addKeyListener(this);
        setPreferredSize(new Dimension(600, 700));
        setVisible(true);
        skeletonList.add(skeleton1);
        skeletonList.add(skeleton2);
        setKeyForSkeletons(skeletonList);
    }

    private void drawBoard(int[][] board, ArrayList<GameObject> gameObjects) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == 0) {
                    gameObjects.add(new Wall(j, i));
                } else {
                    gameObjects.add(new Floor(j, i));
                }
            }
        }
    }

    //this is the method which actually checks the board and sets all visited cell to 1 which represents floor
    private void getRegionSize(int[][] board, int row, int column) {
        if (row < 0 || column < 0 || row >= board.length || column >= board[row].length) {
            return;
        }
        if (board[row][column] == 0 || board[row][column] == 1) {
            return;
        }
        board[row][column] = FLOOR;

        for (int r = row - 1; r <= row + 1; r = r + 2) {
            getRegionSize(board, r, column);
        }
        for (int c = column - 1; c <= column + 1; c = c + 1) {
            getRegionSize(board, row, c);
        }
    }

    private void setKeyForSkeletons(List<Skeleton> list) {
        for (Skeleton s : list) {
            s.setHasKey(false);
        }
        int r = dice.doRandom(list.size() - 1);
        list.get(r).setHasKey(true);
    }

    private void drawCharacters(List<Skeleton> list, Graphics graphics) {
        for (Skeleton s : list
        ) {
            if (s.getHealthPoint() > 0) {
                s.draw(graphics);
            }
            if (hero.getHealthPoint() > 0) {
                hero.draw(graphics);
            }
            if (boss.getHealthPoint() > 0) {
                boss.draw(graphics);
            }
        }
    }

    private void drawTexts(List<Skeleton> list, Graphics graphics) {
        graphics.drawString("Hero " + "(Level " + hero.getLevel() + " ) HP: " + hero.getHealthPoint() + "/" +
                hero.getMaxHP() + " | " + "DP: " + hero.getDefendPoint() + " | " + "SP: " + hero.getStrikePoint(), 200, 610);
        for (Skeleton s : list
        ) {
            if (hero.getPosX() == s.getPosX() && hero.getPosY() == s.getPosY() && s.getHealthPoint() > 0) {
                graphics.drawString("skeleton " + "HP: " + s.getHealthPoint() + "/"
                        + " | " + "DP: " + s.getDefendPoint() + " | " + "SP: " + s.getStrikePoint() + " | " + "Key: " + s.hasKey(), 200, 630);

            }
        }
        if (hero.getPosX() == boss.getPosX() && hero.getPosY() == boss.getPosY() && boss.getHealthPoint() > 0) {
            graphics.drawString("boss " + "HP: " + boss.getHealthPoint() + "/"
                    + " | " + "DP: " + boss.getDefendPoint() + " | " + "SP: " + boss.getStrikePoint(), 200, 630);
        }
    }

    //here I create a n*n board, where 2 represents floor tile and 0 represents wall
    private int[][] createBoard() {
        int[][] randomBoard = new int[10][10];
        dice = new Dice();
        do {
            for (int c = 0; c < 10; c++) {
                for (int r = 0; r < 10; r++) {

                    int d = dice.doRandom(10); /*rand.nextInt(10);*/
                    if (d <= 6) {
                        randomBoard[r][c] = INITIAL_FLOOR;
                    } else randomBoard[r][c] = WALL;
                }
            }
        } while (numberOfRegions(randomBoard) != 1);
        return randomBoard;
    }

    //in this method I determine if board1 created in method createBoard is suitable or not
    //board1 must not have floor tiles that are not connected to other floor tiles, so players can jump on every floor tile
    private int numberOfRegions(int[][] randomBoard) {
        int numberOfRegions = 0;

        for (int c = 0; c < 10; c++) {
            for (int r = 0; r < 10; r++) {
                if (randomBoard[r][c] == INITIAL_FLOOR) {
                    getRegionSize(randomBoard, r, c);
                    numberOfRegions = numberOfRegions + 1;
                }
            }
        }
        return numberOfRegions;
    }
}
