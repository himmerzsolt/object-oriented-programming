package com.matritel.rpg_game.board;

class Floor extends GameObject {
    Floor(int posX, int posY) {
        super("floor.png", posX, posY);
    }
}