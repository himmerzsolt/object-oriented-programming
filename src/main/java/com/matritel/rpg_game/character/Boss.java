package com.matritel.rpg_game.character;

import com.matritel.rpg_game.util.Dice;

public class Boss extends Enemy {
    public Boss(int posX, int posY) {
        super("boss.png", posX, posY);
        this.dice = new Dice();
        int d6 = dice.doRandom(6) + 1;
        this.setHealthPoint((2 * d6) + d6);
        this.setDefendPoint((0.5 * d6) + ((double) d6) / 2);
        this.setStrikePoint(d6 + 1);
    }

    public void reset(int[][] board, int area) {

        int d6 = this.dice.doRandom(6) + 1;
        this.setHealthPoint(2 * area * d6 + d6);
        this.setDefendPoint((double) area / 2 * d6 + (double) d6 / 2);
        this.setStrikePoint(area * d6 + area);
        this.resetPosition(board);
    }
}

