package com.matritel.rpg_game.character;

import com.matritel.rpg_game.board.GameObject;
import com.matritel.rpg_game.util.Dice;

public abstract class Character extends GameObject {
    Dice dice;
    private double defendPoint;
    private double strikePoint;
    private double healthPoint;

    Character(String filename, int posX, int posY) {
        super(filename, posX, posY);
    }

    public double getDefendPoint() {
        return defendPoint;
    }

    public void setDefendPoint(double defendPoint) {
        this.defendPoint = defendPoint;
    }

    public double getStrikePoint() {
        return strikePoint;
    }

    public void setStrikePoint(double strikePoint) {
        this.strikePoint = strikePoint;
    }

    public double getHealthPoint() {
        return healthPoint;
    }

    void setHealthPoint(double healthPoint) {
        this.healthPoint = healthPoint;
    }

    public void strike(Character c) {
        int d6 = dice.doRandom(6) + 1;
        double sv = 2 * d6 * this.strikePoint;
        if (sv > c.defendPoint) {
            c.healthPoint = c.healthPoint - (sv - c.defendPoint);
        }
    }

    public void resetPosition(int[][] board) {
        int newY;
        int newX;
        do {
            int extraX = this.dice.doRandom(10);
            int extraY = this.dice.doRandom(10);
            newX = this.getPosX() + extraX;
            newY = this.getPosY() + extraY;
            //make sure not to leave the area/field/board
            if ((newX > -1 && newY > -1) && (newX < 10 && newY < 10)) {
                //make sure it cannot step on wall
                if (board[newY][newX] == 1) {
                    this.setPosX(newX);
                    this.setPosY(newY);
                    break;
                }
            }
        } while (board[this.getPosY()][this.getPosX()] == 0);
    }
}
