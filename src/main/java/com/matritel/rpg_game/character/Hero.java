package com.matritel.rpg_game.character;

import com.matritel.rpg_game.util.Dice;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class Hero extends Character {
    private double maxHP;
    private int level;

    public Hero(int posX, int posY) {
        super("hero-down.png", posX, posY);
        this.level = 1;
        this.dice = new Dice();
        int d6 = dice.doRandom(6) + 1;
        this.setHealthPoint(20 + (3 * d6));
        this.maxHP = this.getHealthPoint();
        this.setStrikePoint(2 * d6);
        this.setDefendPoint(5 + d6);
    }

    public double getMaxHP() {
        return maxHP;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setMax_hp(double max_hp) {
        this.maxHP = max_hp;
    }

    public void changePic(String direction) {
        switch (direction) {
            case "left":
                try {
                    image = ImageIO.read(new File("hero-left.png"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case "right":
                try {
                    image = ImageIO.read(new File("hero-right.png"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "down":
                try {
                    image = ImageIO.read(new File("hero-down.png"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case "up":
                try {
                    image = ImageIO.read(new File("hero-up.png"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void move(int extraX, int extraY, int[][] board) {
        int newX = this.getPosX() + extraX;
        int newY = this.getPosY() + extraY;
        //make sure not to leave the area/field/board
        if ((newX > -1 && newY > -1) && (newX < 10 && newY < 10)) {
            //make sure it cannot step on wall
            if (board[newY][newX] == 1) {
                this.setPosX(newX);
                this.setPosY(newY);
            }
        }
    }
}

