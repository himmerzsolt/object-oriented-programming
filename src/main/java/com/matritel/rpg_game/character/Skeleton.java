package com.matritel.rpg_game.character;

import com.matritel.rpg_game.util.Dice;

public class Skeleton extends Enemy {
    private boolean hasKey;

    public Skeleton(int posX, int posY, int area) {
        super("skeleton.png", posX, posY);
        this.dice = new Dice();
        int d6 = dice.doRandom(6) + 1;
        this.setHealthPoint(2 * area * d6);
        this.setDefendPoint((double) area / 2 * d6);
        this.setStrikePoint(area * d6);
        this.hasKey = false;
    }

    public boolean hasKey() {
        return hasKey;
    }

    public void setHasKey(boolean hasKey) {
        this.hasKey = hasKey;
    }

    public void reset(int[][] board, int area) {

        int d6 = this.dice.doRandom(6) + 1;
        this.setHealthPoint(2 * area * d6);
        this.setDefendPoint((double) area / 2 * d6);
        this.setStrikePoint(area * d6);
        this.resetPosition(board);
    }
}

