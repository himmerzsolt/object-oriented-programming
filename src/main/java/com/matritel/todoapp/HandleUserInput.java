package com.matritel.todoapp;

import java.util.ArrayList;
import java.util.Scanner;

class HandleUserInput {
    private boolean isItDone = true;
    void start(){
        ArrayList<ToDoList> mainList = new ArrayList<>();
        //ToDoList listOfItems = new ToDoList();
        help();
        while (isItDone) {
            obtainUserInput(mainList);
        }
    }

    private String stringScanner(){
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    private int numberScanner() {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    private void help(){
        System.out.println("Select an option!");
        System.out.println();
        System.out.println("Type 'list' to create a new list.");
        System.out.println("\tShorthand for list: 'l'");
        System.out.println("Type 'print' to list all the tasks.");
        System.out.println("\tShorthand for list: 'p'");
        System.out.println("Type 'add' to add a new task.");
        System.out.println("\tShorthand for add: 'a'");
        System.out.println("Type 'remove' to remove a task.");
        System.out.println("\tShorthand for remove: 'r'");
        System.out.println("Type 'complete' to make a task completed.");
        System.out.println("\tShorthand for complete: 'c'");
        System.out.println("Type 'help' to print out this list again.");
        System.out.println("\tShorthand for help: 'h'");
        System.out.println("Type 'exit' to quit.");
        System.out.println("\tShorthand for exit: 'e'");
    }

    private void obtainUserInput(ArrayList<ToDoList> mainList){

        String inputString = stringScanner();

        if ("print".equals(inputString) || "p".equals(inputString)){
            System.out.println("here are all your lists:");
            for (int i=0; i<mainList.size(); i++) {
                System.out.println( i+1 + " "+ mainList.get(i).getName());
            }
            System.out.println("select the index of the list you wish to print");
            int i = numberScanner();
            mainList.get(i-1).printList();
            help();
        } else if ("list".equals(inputString) || "l".equals(inputString)) {
            System.out.println("add the name of the new list");
            String listName = stringScanner();
            ToDoList newList = new ToDoList();
            newList.setName(listName);
            System.out.println(listName + " list is created");
            mainList.add(newList);
        }
        else if ("add".equals(inputString) || "a".equals(inputString)){
            boolean isContinued = true;
            System.out.println("Select the index of the list");
            for (int i=0; i<mainList.size(); i++) {
                System.out.println( i+1 + " "+ mainList.get(i).getName());
            }
            int index = numberScanner();
            mainList.get(index-1).addNewItem();
            while (isContinued) {
                System.out.println("Would yo like to add new item? y/n");
                String ui = stringScanner();
                if ("y".equals(ui)){
                    mainList.get(index-1).addNewItem();
                }else if ("n".equals(ui)){
                    isContinued = false;
                    System.out.println("Your new "+ mainList.get(index-1).getName()+"list is:");
                    mainList.get(index-1).printList();

                    System.out.println("Would you like to do something else? y/n");
                    String ui1 = stringScanner();
                    if ("y".equals(ui1)){
                        help();
                    }else if ("n".equals(ui1)){
                        exitApp();
                    }else System.out.println("The proper input is y/n");
                }else System.out.println("The proper input is y/n");
            }
        }else if ("remove".equals(inputString) || "r".equals(inputString)){
            boolean isContinued = true;
            System.out.println("which list would you like to edit?");
            for (int i=0; i<mainList.size(); i++) {
                System.out.println( i+1 + " "+ mainList.get(i).getName());
            }
            int i = numberScanner();
            mainList.get(i-1).printList();
            mainList.get(i-1).removeItem();
            while (isContinued){
                System.out.println("Would yo like to remove an other item? y/n");
                String ui = stringScanner();
                if (ui.equals("y")){
                    mainList.get(i-1).removeItem();
                }else if (ui.equals("n")){
                    isContinued = false;
                    System.out.println("Your new " + mainList.get(i-1).getName()+"list is:");
                    mainList.get(i-1).printList();
                    System.out.println("Would you like to do something else? y/n");
                    String ui1 = stringScanner();
                    if ("y".equals(ui1)){
                        help();
                    }else if ("n".equals(ui1)){
                        exitApp();
                    }else System.out.println("The proper input is y/n");
                }
            }
        }else if ("complete".equals(inputString) || ("c".equals(inputString))){
            System.out.println("which list would you like to edit?");
            for (int i=0; i<mainList.size(); i++) {
                System.out.println( i+1 + " "+ mainList.get(i).getName());
            }
            int index = numberScanner();
            mainList.get(index-1).printList();
            mainList.get(index-1).setItCompleted();
            System.out.println("Your new "+ mainList.get(index-1).getName() +" list is:");
            mainList.get(index-1).printList();
            System.out.println("Would you like to do something else? y/n");
            String ui = stringScanner();
            if ("y".equals(ui)){
                help();
            }else if ("n".equals(ui)){
                exitApp();
            }else System.out.println("The proper input is y/n");

        }else if ("help".equals(inputString) || ("h".equals(inputString))){
            help();
        }else if ("exit".equals(inputString) || ("e".equals(inputString))){
            exitApp();
        }else System.out.println("Input is not correct. Try again");
    }

    private void exitApp(){
        isItDone = false;
        System.out.println("Thank you for using the ToDo app. See you next time!");

    }
}
