package com.matritel.todoapp;

public class ToDoItem {
    private String item;
    private boolean isCompleted;

    ToDoItem(String item) {
        this.item = item;
    }

    String getItem() {
        return item;
    }

    boolean isCompleted() {
        return isCompleted;
    }

    void setItem(String item) {
        this.item = item;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

}
