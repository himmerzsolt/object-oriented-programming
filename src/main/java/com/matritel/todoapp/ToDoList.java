package com.matritel.todoapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class ToDoList {
    private String name;
    private List<ToDoItem> listOfItems = new ArrayList<>();

    private int numberScanner() {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    void printList () {
        for (int i = 0; i < listOfItems.size(); i++) {
            System.out.print(i + 1 + "\t" + listOfItems.get(i).getItem() + "\t");
            if (listOfItems.get(i).isCompleted()) {
                System.out.println("[X]");
            } else
                System.out.println("[ ]");
        }
    }

    void addNewItem () {
        System.out.println("Add new item(s) to the list");
        Scanner sc = new Scanner(System.in);
        String inputString = sc.nextLine();
        ToDoItem newItem = new ToDoItem(inputString);
        listOfItems.add(newItem);
        System.out.println("New item was added");
    }

    void removeItem () {
        System.out.println("Please enter the number of the list element you want to remove from the list \n");
        int numberOfItem = numberScanner();
        while (numberOfItem>listOfItems.size() || numberOfItem <1){
            System.out.println("Number is not in the list");
            numberOfItem = numberScanner();
        }
        listOfItems.remove(numberOfItem - 1);
        System.out.println("item succesfully removed from the list");
    }

    void setItCompleted () {
        System.out.println("Please enter the number of the list element you want to set completed \n");
        Scanner sc = new Scanner(System.in);
        int numberOfItem = sc.nextInt();
        listOfItems.get(numberOfItem - 1).setCompleted(true);
        System.out.println("item set completed");
    }
    void setName(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}
